import { dataShoe } from "../DataShoe";
import ItemShoe from "../ItemShoe";
import { ADD_TO_CART, CHANGE_AMOUNT, VIEW_DETAIL } from "./constant/constant";

let initialState = {
  shoeArr: dataShoe,
  detail: dataShoe[0],
  cart: [],
};

export const shoeShopReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = state.cart.findIndex((item) => {
        return item.id == payload.id;
      });
      if (index == -1) {
        let cartitem = { ...payload, number: 1 };
        cloneCart.push(cartitem);
      } else {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    case VIEW_DETAIL: {
      let index = payload.index;
      return { ...state, detail: dataShoe[index] };
    }
    case CHANGE_AMOUNT: {
      let cloneCart = [...state.cart];
      let index = payload.indexShoe;
      if (index !== -1) {
        cloneCart[index].number += payload.amount;
      }
      cloneCart[index].number == 0 && cloneCart.splice(index, 1);

      return { ...state, cart: cloneCart };
    }
    default:
      return { ...state };
  }
};
