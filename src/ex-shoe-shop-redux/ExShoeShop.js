import { Component } from "react";
import Cart from "./Cart";

import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class ExShoeShop extends Component {
  render() {
    return (
      <div className="container py-5">
        <Cart />
        <ListShoe />
        <DetailShoe />
      </div>
    );
  }
}
