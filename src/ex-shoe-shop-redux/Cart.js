import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_AMOUNT } from "./redux/constant/constant";

class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img style={{ width: "50px" }} src={item.image} alt="" />
          </td>
          <td>{item.price * item.number}$</td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeItemAmount(index, -1);
              }}
              className="btn btn-danger"
            >
              <i class="fa fa-minus-circle"></i>
            </button>
            {item.number}
            <button
              onClick={() => {
                this.props.handleChangeItemAmount(index, 1);
              }}
              className="btn btn-success"
            >
              <i class="fa fa-plus-square"></i>
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Image</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cart: state.shoeShopReducer.cart,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleChangeItemAmount: (indexShoe, amount) => {
      dispatch({
        type: CHANGE_AMOUNT,
        payload: {
          indexShoe: indexShoe,
          amount: amount,
        },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
